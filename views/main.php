<!DOCTYPE html>
<html>
<head>
<?php visual::renderHead() ?>
</head>
<body>

<div id="nav-panel">
<?php
use \widgets\ActiveMenu;
$a = new ActiveMenu;
$a->add('Главная','?r=index','index');
$a->add('Тестирование','?r=test','test');
if(!app::$user || !app::$user->isAuth)
  $a->add('Вход','?r=index&a=auth','index_auth');
else 
  $a->add('Выход('.app::$user->login.')','?r=index&a=exit','index_exit');
$a->setActive(visual::$activeid,true);
$a->printHtml();
?>
</div>
<div id="maindiv">
<?php visual::renderBody() ?>
</div>
</body>
</html>
